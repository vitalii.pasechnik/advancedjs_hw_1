
export class Employee {
   constructor(name, age, salary) {
      this.name = name;
      this.age = age;
      this.salary = salary;
   }

   get name() {
      return this._name;
   }

   set name(value) {
      if (/(\W|\d)/g.test(value)) {
         console.warn("Ви ввели не коректне ім'я");
      }
      this._name = value;
   }
   get age() {
      return this._age;
   }

   set age(value) {
      if (/\D/g.test(value)) {
         console.warn("Ви ввели не коректний вік");
      }
      this._age = value;
   }

   get salary() {
      return this._salary + `$`;
   }

   set salary(value) {
      if (/\D/g.test(value)) {
         console.warn("Ви ввели не коректну суму");
      }
      this._salary = value;
   }
}