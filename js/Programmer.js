import { Employee } from "./Employee.js";


export class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    set lang(value) {
        this._lang = value;
    }

    get lang() {
        return this._lang;
    }

    get salary() {
        return this._salary * 3 + `$`;
    }

    set salary(value) {
        if (/\D/g.test(value)) {
            console.warn("Ви ввели не коректну суму");
        }
        this._salary = value;
    }
}
